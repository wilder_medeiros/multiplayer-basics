﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using TMPro;

public class MyNetworkPlayer : NetworkBehaviour
{
    [SerializeField] Renderer displayColorRenderer = null;
    [SerializeField] TMPro.TMP_Text displayNameText = null;

    [SyncVar(hook = nameof(HandleDisplayNameUpdated))]
    [SerializeField] string displayName = "Missing name";

    [SyncVar(hook = nameof(HandleDisplayColorUpdated))]
    [SerializeField] Color displayColor = Color.black;

    [Server]
    public void SetDisplayName(string newDisplayName)
    {
        displayName = newDisplayName;
    }

    [Server]
    public void SetDisplayColor(Color newDisplayColor) 
    {
        displayColor = newDisplayColor;
    }

    void HandleDisplayNameUpdated(string oldName, string newName)
    {
        displayNameText.text = newName;
    }

    void HandleDisplayColorUpdated(Color oldColor, Color newColor)
    {
        displayColorRenderer.material.SetColor("_BaseColor", newColor);
    }
}
